import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import neighbors
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn import metrics

sys.stdout = open('metrics_knn', 'w')
scaler = MinMaxScaler()
dataset = pd.read_csv(r"C:\Users\andrei\Desktop\AndreiLocal\NUIG\4thYr_70\ML\Assignment3\steel.csv")

X = dataset[['normalising_temperature', 'tempering_temperature', 'percent_silicon', 'percent_chromium',
                 'manufacture_year', 'percent_copper', 'percent_nickel', 'percent_sulphur', 'percent_carbon',
                 'percent_manganese']]
y = dataset['tensile_strength']

X_n = pd.DataFrame(data=scaler.fit_transform(X), columns=X.columns)

X_train, X_test, y_train, y_test = train_test_split(X_n, y, test_size=0.2, random_state=0)  # random 80/20 split for train/test

rmse_vals = []  # array for RMSE values for different values of k
for k in range(1, 21):
    model = neighbors.KNeighborsRegressor(n_neighbors=k)

    model.fit(X_train, y_train)  # fit the model

    r2 = model.score(X, y)

    print("----------------------------------------------------------------")
    print('r2 value for k = ' + str(k) + ' is: ' + str(r2))

    pred = model.predict(X_test)
    error = np.sqrt(metrics.mean_squared_error(y_test, pred))
    rmse_vals.append(error)
    print('RMSE val for k = ' + str(k) + ' is: ' + str(error))

k_vals = np.arange(1,21)
plt.plot(k_vals, rmse_vals)
plt.title('RMSE vs k_vals')
plt.xlabel('k')
plt.ylabel('RMSE')
plt.show()




