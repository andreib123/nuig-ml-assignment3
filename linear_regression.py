import sys
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics
from sklearn.decomposition import PCA
import numpy as np


def linear_regression_all(regressor, dataset, normalisedX, pca):
    y = dataset['tensile_strength']
    X = dataset[['normalising_temperature', 'tempering_temperature', 'percent_silicon', 'percent_chromium',
                 'manufacture_year', 'percent_copper', 'percent_nickel', 'percent_sulphur', 'percent_carbon',
                 'percent_manganese']]

    if normalisedX:
        X = pd.DataFrame(data=scaler.fit_transform(X), columns=X.columns)

    if pca:
        pca = PCA(n_components=6)
        principalComponents = pca.fit_transform(X)
        X = pd.DataFrame(data=principalComponents)
        v = pca.explained_variance_ratio_
        print("variance explained by each principal component: " + str(v))
        print("total variance explained by all principal components: " + str(sum(v)*100) + "%")

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
    regressor.fit(X_train, y_train)

    r2 = regressor.score(X, y)
    print("r2: " + str(r2))

    coeff_df = pd.DataFrame(regressor.coef_, X.columns, columns=['Coefficient'])
    print(coeff_df)

    y_p = regressor.predict(X_test)
    y_yp_df = pd.DataFrame({'Actual': y_test, 'Predicted': y_p})
    y_yp_df25 = y_yp_df.head(25)  # first 25 y vs y_p
    y_yp_df25.plot(kind='bar', figsize=(10, 8))
    plt.grid(which='major', linestyle='-', linewidth='0.5', color='green')
    plt.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
    plt.title('actual vs predicted tensile_strength')
    plt.xlabel('testing input number')
    plt.ylabel('tensile_strength')
    plt.show()

    print('RMSE:', np.sqrt(metrics.mean_squared_error(y_test, y_p)))


def linear_regression_one(regressor, feature, dataset):
    X = dataset[feature].values
    y = dataset['tensile_strength'].values
    X = X.reshape(-1, 1)
    y = y.reshape(-1, 1)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)
    regressor.fit(X_train, y_train)
    r2 = regressor.score(X, y)

    y_p = regressor.predict(X_test)
    slope = regressor.coef_
    intercept = regressor.intercept_

    plt.scatter(X_train, y_train)
    axes = plt.gca()
    x_vals = axes.get_xlim()
    y_vals = intercept + slope * x_vals
    y_vals = y_vals.ravel()

    plt.plot(x_vals, y_vals, color='red')
    plt.title('tensile_strength vs {0}'.format(feature))
    plt.xlabel('{0}'.format(feature))
    plt.ylabel('tensile_strength')
    plt.show()

    print("----------------------------------------------------------------")
    print(feature + " vs tensile_strength:\n")
    print("r2: " + str(r2))
    print("Intercept: %8.4f" % (intercept))
    print("Coefficient: %8.4f" % (slope))
    print('RMSE:', np.sqrt(metrics.mean_squared_error(y_test, y_p)))


sys.stdout = open('metrics_lr', 'w')
scaler = MinMaxScaler()
regressor = LinearRegression()

dataset = pd.read_csv(r"C:\Users\andrei\Desktop\AndreiLocal\NUIG\4thYr_70\ML\Assignment3\steel.csv")
features = dataset.columns[0:11]

#for feature in features:
#    linear_regression_one(regressor, feature, dataset)

linear_regression_all(regressor, dataset, True, False)

